# -*- mode: ruby -*-
# vi: set ft=ruby :

### Custom vars ###
config_name = "ubuntu18.04-dev"
storage_size = "20GB"
memory_mb = "4096"
cpu_num = "2"
private_ip = "192.168.50.4"
timezone = "America/Denver"

# Sent via ssh on each connection to guest from host
forward_vars = [
  "AWS_*",
  "VAULT_*",
  "VSPHERE_*",
]

# Strings to not be printed or logged by vagrant
sensitive_strings = [
  ENV["AWS_ACCESS_KEY_ID"],
  ENV["AWS_SECRET_ACCESS_KEY"],
  ENV["VSPHERE_PASSWORD"],
  ENV["VAULT_TOKEN"]
]


### https://www.vagrantup.com/docs/vagrantfile ###

Vagrant.require_version ">= 2.2.6"

Vagrant.configure("2") do |config|

  ### Dev machine definition and config
  config.vm.define config_name
  config.vm.box = "ubuntu/bionic64"
  config.vm.hostname = "ubuntu-dev"

  # VAGRANT_EXPERIMENTAL="disk_base_config" must be set while this is experimental
  # https://www.vagrantup.com/docs/disks/
  config.vm.disk :disk, primary: true, size: storage_size

  # config.vm.network "forwarded_port", guest: 8000, host: 8000, host_ip: "127.0.0.1"
  # config.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.1"
  config.vm.network "private_network", ip: private_ip

  config.vm.provider "virtualbox" do |vb|
    vb.name = config_name
    # time sync when off by 10s
    vb.customize [ "guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 1000 ]
    vb.memory = memory_mb
    vb.cpus = cpu_num
  end
  
  ### Provisioning
  config.vm.synced_folder "#{ENV['HOME']}", "/host_home"

  aws_dir = "#{ENV['HOME']}/.aws"
  if Dir.exist?(aws_dir)
    config.vm.synced_folder aws_dir, "/home/vagrant/.aws"
  end

  if File.exists?("./config/bash_profile")
    config.vm.provision "profile", type: "file", source: "./config/bash_profile", 
      destination: "/home/vagrant/.bash_profile", run: "always"
  end
  
  config.vm.provision "keys", type: "shell", path: "./scripts/copy_ssh_keys.sh", privileged: false, run: "always"
  config.vm.provision "fwd_vars", type: "shell", path: "./scripts/sshd_setup.sh", args: forward_vars, run: "always"

  config.vm.provision "deps", type: "shell", path: "./scripts/provision_dev_vm.sh", run: "once",
    privileged: false, env: {"TZ" => timezone}

  config.vm.provision "tools", type: "shell", path: "./scripts/devtools_setup.sh", run: "once",
    privileged: false, env: {"GOLANG_VERSION" => nil, "PYTHON_VERSION" => nil, "TF_VERSION" => nil,  "GIT_USER" => nil, "GIT_EMAIL" => nil}

  config.vm.provision "extras", type: "shell", path: "./scripts/extra_setup.sh", privileged: false, run: "never"

  ### SSH Settings
  config.ssh.forward_agent = false
  config.ssh.verify_host_key = :accept_new_or_local_tunnel
  config.ssh.forward_env = forward_vars

  config.vagrant.sensitive = sensitive_strings

end
