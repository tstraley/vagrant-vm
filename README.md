# Vagrant VMs

Files and scripts associated with setting up dev VMs using Vagrant and setup scripts.

## Vagrant Quick Start

[Vagrant Quick Start Guide](https://www.vagrantup.com/intro/getting-started/index.html)

#### Install
[Install Vagrant from their downloads page](https://www.vagrantup.com/docs/installation/)

[Install VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Custom disk size for the guest is [currently experimental](https://www.vagrantup.com/docs/disks/)
and requires `VAGRANT_EXPERIMENTAL="disk_base_config"` exported in your environment.

#### Configure
Review the configuration in the [Vagrantfile](Vagrantfile) and the [config](config/README.md) 
directory. Make any changes you see fit.

> Reference:
> - https://www.vagrantup.com/docs/vagrantfile
> - [Configuration README](config/README.md)

#### Start up
In the main directory (where the Vagrantfile is), you can simply run:
```shell
    vagrant up
```

After everything is provisioned, you can login to the new VM:
```shell
    vagrant ssh
```

#### Tear down
To stop the VM, run either of the following:
```shell
    vagrant suspend   #Stores the current vRAM state - will have faster startup
    vagrant halt   #Shuts down the guest OS - longer startup
```

To completely remove the VM (you are completely done with it)
```shell
    vagrant destroy
```

## Provisioning

The vagrant VM is configured to mount some shared directories.

The Vagrantfile includes several provisioning stages, which will automatically run when starting up the VM.

Following startup, other setup scripts are available within the VM (`/vagrant/scripts`), or through manual provision calls.

### Synced (Mounted) Directories

Host Machine | Vagrant VM         | Purpose
------------ | ------------------ | -------
./           | /vagrant           | Vagrant VM always mounts the present working directory where the Vigrant file lives by default. We use this for the additional provisioning scripts, and grabbing config files.
~/           | /host_home         | Mount the underlying home directory. Used for sharing user files and git repos into the VM.
~/.aws/      | /home/vagrant/.aws | Include the host AWS credentials, certs, and/or profiles if a user has them in this location.

### Provision Stages

On initialization (first time startup), all "always" and "once" provisioning tasks will be run. After that,
if restarting the VM only the "always" tasks will be re-run automatically.

Name    | Type  | Run Schedule | Purpose
------- | ----- | ------------ | -------
profile | file  | always  | Places the [bash_profile](config/README.md) script (if exists) into ~/.bash_profile for providing aliases and environment variables.
keys    | shell | always  | Copies ssh keys from the host user to the vagrant user (`~/.ssh/id_*`). Executed via [copy_ssh_keys.sh](scripts/copy_ssh_keys.sh)
deps    | shell | once    | Perform initial setup and install some basic packages. Executed via [provision_dev_vm.sh](scripts/provision_dev_vm.sh).
tools   | shell | once    | Install various tools for dev and product work: eg. docker, terraform, golang. This will include pip packages defined in [requirements.txt](config/README.md)
fwd_vars| shell | always  | Ensures that all configured `forward_vars` are accepted by the VM's sshd service.
extras  | shell | manual  | Executes a custom `extra_setup.sh` script in the scripts directory.

In order to execute the "manual" stages (or any specific provision stage at any time), use one of the following commands with the provision stage name, or provision type (can be comma-delineated list):
```shell
    vagrant provision --provision-with <names_or_types>
    
    vagrant up --provision-with <names_or_types>

    vagrant reload --provision-with <names_or_types>
```

You can re-trigger the default provisioning stages with one of the following commands
(these include provisioning stages that are typically only run "once"):
```shell
    # re-run the "always" and "once" provisioners
    vagrant provision

    # when starting the VM after halt or suspend
    vagrant up --provision

    # when restarting the VM (possibly with Vagrantfile changes)
    vagrant reload --provision
```

To startup or reload without running any provisioning stages, run either of the following:
```shell
    vagrant up --no-provision
    
    vagrant reload --no-provision
```
