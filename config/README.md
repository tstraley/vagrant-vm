# Dev VM Configuration

Most configuration for these dev VMs can be controlled with
the [Vagrantfile](../Vagrantfile). Please review the official
[vagrant documentation](https://www.vagrantup.com/docs/vagrantfile)
for details and additional settings.

This doc will cover items in this `config` directory.

### Config Items

These are the current supported config files. They must live in this directory.
The provision stages that use these config files can be found in the
[provision section of the project README](../README.md#provision-stages).

There are examples available in the `examples` directory.

Filename           | Requisiteness | Provision Stage | Description
------------------ | ------------- | --------------- | -----------
timesyncd.conf     | Required      | deps    | Systemd [TimeSync service config](https://www.freedesktop.org/software/systemd/man/timesyncd.conf.html), necessary for ensuring guest VM has best ability to keep its clock correct and accurate. You probably don't need to change this.
bash_profile       | Optional      | profile | A user [bash_profile](http://www.linuxfromscratch.org/blfs/view/6.3/postlfs/profile.html) which will be sourced when logging in as the vagrant user. Set any environment variables, aliases, functions and other shell settings here.
docker_daemon.json | Recommended   | tools   | The [docker daemon configuration](https://docs.docker.com/config/daemon/#configure-the-docker-daemon) file. Applied when docker is installed on the guest VM.
gitconfig          | Recommended   | deps    | The user global [git config](https://www.git-scm.com/book/en/v2/Customizing-Git-Git-Configuration) file. Git does not work without at least a couple of user config items set. This will *not* override an existing ~/.gitconfig file found on the guest.
requirements.txt   | Optional      | tools   | A pip (python3) [requirements file](https://pip.readthedocs.io/en/1.1/requirements.html). Add any additional tools and packages that you want pip to manage for you.
