#!/usr/bin/env bash

### Any forwarded vars passed as args will be applied to the sshd
### Run as root

set -eo pipefail

cfg=/etc/ssh/sshd_config
for var in "$@"; do
    if ! grep "$var" "$cfg"; then
        echo "AcceptEnv $var" | tee -a "$cfg"
    fi
done
systemctl restart sshd