#!/usr/bin/env bash

### Copy any ssh keys found in host user's .ssh dir to vagrant's .ssh dir
### Only overriding if newer (and backing up whenever replaced)

set -eo pipefail

ssh_dir=/host_home/.ssh
if ! [ -d "$ssh_dir" ]; then
    printf "\nERROR: %s not found.\n" "$ssh_dir" >&2
    exit 1
fi

shopt -s nullglob
mkdir -p "${HOME}/.ssh"
for key in "$ssh_dir"/id_*; do
    basename "$key"
    cp -ua --backup=numbered "$key" "${HOME}/.ssh/"
done