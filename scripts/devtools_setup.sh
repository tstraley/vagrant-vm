#!/usr/bin/env bash

### Installs python3, docker, kubectl, golang, terraform, and any configured pip packages
### Configures git

set -eo pipefail

GOLANG_VERSION="${GOLANG_VERSION:-1.13.10}"
PYTHON_VERSION="${PYTHON_VERSION:-3.8}"
TF_VERSION="${TF_VERSION:-0.12.24}"

configure_git() {
	host_cfg=/vagrant/config/gitconfig
	guest_cfg="${HOME}/.gitconfig"
	if [ -f "$host_cfg" ]; then
		if [ -f "$guest_cfg" ] && ! cmp -s "$host_cfg" "$guest_cfg"; then
			printf "\nWARN: %s already exists; not overwriting with %s.\n" "$guest_cfg" "$host_cfg" >&2
		else
			cp -a "$host_cfg" "$guest_cfg"
		fi
		return 0
	fi
	[ -n "$GIT_USER" ] && git config --global user.name "$GIT_USER"
	[ -n "$GIT_EMAIL" ] && git config --global user.email "$GIT_EMAIL"
	return 0
}

install_python() {
    sudo apt-get update -yq
    sudo DEBIAN_FRONTEND=noninteractive apt-get install -yq \
    	"python${PYTHON_VERSION}" \
		python3-pip
    
	sudo ln -sf "/usr/bin/python${PYTHON_VERSION}" /usr/bin/python3
}

install_docker() {
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88 | grep '9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88' >/dev/null
	echo "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list
	sudo apt-get update -yq
	sudo apt-get install -yq docker-ce docker-ce-cli containerd.io
	sudo usermod -aG docker "$USER"
}

install_kubectl() {
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
	echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
	sudo apt-get update -yq
    sudo apt-get install -yq kubectl
}

install_golang() {
	cd /tmp
	wget -q "https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz"
	tar -xf "go${GOLANG_VERSION}.linux-amd64.tar.gz"
	sudo mv go "$GOROOT"
	sudo rm -rf ./go*
	cd -
	mkdir -p "$GOPATH" "$GOPATH/src" "$GOPATH/pkg" "$GOPATH/bin"
}

install_terraform() {
    cd /tmp
    wget -q "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip"
    unzip "terraform_*.zip"
    sudo mv terraform /usr/local/bin/
    sudo rm -rf terraform*
    cd -
}

configure_git
if ! command -v "python${PYTHON_VERSION}" >/dev/null; then
    install_python
fi
if ! command -v docker >/dev/null; then
    install_docker
fi
if [ -f /vagrant/config/docker_daemon.json ]; then
    sudo cp -f /vagrant/config/docker_daemon.json /etc/docker/daemon.json
    sudo systemctl restart docker
fi

if ! command -v kubectl >/dev/null; then
    install_kubectl
fi
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl >/dev/null

if ! command -v go >/dev/null; then
    install_golang
fi

if ! command -v terraform >/dev/null; then
    install_terraform
fi

if [ -f /vagrant/config/requirements.txt ]; then
    pip install -r /vagrant/config/requirements.txt
fi

sudo apt-get clean