#!/usr/bin/env bash

### VM initialization tasks and installs

set -eo pipefail

## Functions
install_basics() {
	sudo apt-get update -yq
	sudo DEBIAN_FRONTEND=noninteractive apt-get install -yq \
		apt-transport-https \
		ca-certificates \
		curl \
		git \
		gnupg-agent \
		gnupg2 \
		jq \
		software-properties-common \
		unzip \
		wget

	sudo apt-get clean
}

setup_timesync() {
	# Set ntp / time appropriately
	sudo cp -fa /vagrant/config/timesyncd.conf /etc/systemd/timesyncd.conf
	sudo systemctl daemon-reload
	sudo timedatectl set-timezone "${TZ:?ERROR: TZ not set, unable to set up timesync.}"
	sudo timedatectl set-ntp true
	sudo systemctl restart systemd-timesyncd
}

### MAIN
install_basics
setup_timesync
